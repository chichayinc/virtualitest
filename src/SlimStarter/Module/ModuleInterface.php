<?php
namespace SlimStarter\Module;

interface ModuleInterface{
    public function getModuleName();
    public function getModuleAccessor();
    public function getTemplatePath();
    public function registerSuperAdminRoute();
    public function registerSuperAdminMenu();
    public function registerAdminRoute();
    public function registerAdminMenu();
    public function registerExaminerRoute();
    public function registerExaminerMenu();
    public function registerExamineeRoute();
    public function registerExamineeMenu();
    public function registerPublicRoute();
    public function registerHook();
    public function boot();
    public function install();
    public function uninstall();
    public function activate();
    public function deactivate();
}