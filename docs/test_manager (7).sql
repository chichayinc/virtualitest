-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2016 at 05:46 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE IF NOT EXISTS `choices` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `choice_text` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `modified_by` varchar(200) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `uuid`, `question_id`, `is_correct`, `choice_text`, `created_by`, `modified_by`, `updated_at`, `created_at`, `is_deleted`) VALUES
(1, 'd76d3966-705f-11e6-8b17-005056b1433f', 1, 0, 'Vibal', '1', '1', '2016-09-01 16:19:21', '2016-09-01 16:19:21', 0),
(2, 'd76d93d7-705f-11e6-8b17-005056b1433f', 1, 0, 'Vibe Technology', '1', '1', '2016-09-01 16:19:21', '2016-09-01 16:19:21', 0),
(3, 'd76dd048-705f-11e6-8b17-005056b1433f', 1, 0, 'VSmart', '1', '1', '2016-09-01 16:19:21', '2016-09-01 16:19:21', 0),
(4, 'd76e1f2f-705f-11e6-8b17-005056b1433f', 1, 1, 'Virtualidad Inc', '1', '1', '2016-09-01 16:19:21', '2016-09-01 16:19:21', 0),
(5, '4a2181a0-7061-11e6-8b17-005056b1433f', 2, 1, 'choice 1', '1', '1', '2016-09-01 16:29:43', '2016-09-01 16:29:43', 0),
(6, '4a21b80a-7061-11e6-8b17-005056b1433f', 2, 0, 'choice 2', '1', '1', '2016-09-01 16:29:43', '2016-09-01 16:29:43', 0),
(7, '4a2206d3-7061-11e6-8b17-005056b1433f', 2, 0, 'choice 3', '1', '1', '2016-09-01 16:29:43', '2016-09-01 16:29:43', 0),
(8, '8b3ceb13-7067-11e6-8b17-005056b1433f', 3, 1, 'choice 1', '1', '1', '2016-09-01 17:14:30', '2016-09-01 17:14:30', 0),
(9, '8b3d24ab-7067-11e6-8b17-005056b1433f', 3, 0, 'choice 2', '1', '1', '2016-09-01 17:14:30', '2016-09-01 17:14:30', 0),
(10, '8b3d6b3d-7067-11e6-8b17-005056b1433f', 3, 0, 'choice 3', '1', '1', '2016-09-01 17:14:30', '2016-09-01 17:14:30', 0);

--
-- Triggers `choices`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_choices` BEFORE INSERT ON `choices`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `group_parent_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `is_approved` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `uuid`, `group_parent_id`, `name`, `permissions`, `is_approved`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'b322cf90-1a1a-40de-91b5-a77d492a2036', 0, 'Administrators', '{"admin_portal":1}', 1, 0, '2015-03-06 02:13:08', '2015-03-06 02:13:08'),
(2, 'b097bf77-706f-11e6-8b17-005056b1433f', 1, 'Government', '{"group_admin":1}', 1, 1, '2016-09-01 18:12:48', '2016-09-01 21:06:58'),
(3, '8f9c20ed-7072-11e6-8b17-005056b1433f', 1, 'Sample 123 Group', '{"group_admin":1}', 1, 0, '2016-09-01 18:33:21', '2016-09-02 00:25:42'),
(4, '1b303a71-707e-11e6-8b17-005056b1433f', 1, 'tes Group', '{"group_admin":1}', 1, 1, '2016-09-01 19:56:00', '2016-09-01 21:08:25'),
(5, 'c69b95c6-707f-11e6-8b17-005056b1433f', 1, 'test123', '{"group_admin":1}', 1, 1, '2016-09-01 20:07:57', '2016-09-02 00:28:24'),
(6, 'fc444e9e-707f-11e6-8b17-005056b1433f', 1, 'test321', '{"group_admin":1}', 1, 1, '2016-09-01 20:09:27', '2016-09-01 21:09:44'),
(7, 'ce11fef7-70a3-11e6-8b17-005056b1433f', 1, 'test 123434354', '{"group_admin":1}', 1, 1, '2016-09-02 00:25:52', '2016-09-02 00:34:11'),
(8, 'f040f787-70a4-11e6-8b17-005056b1433f', 1, '123', '{"group_admin":1}', 1, 1, '2016-09-02 00:33:58', '2016-09-02 00:50:22'),
(9, '2c34431c-70a7-11e6-8b17-005056b1433f', 1, 'eqwreasfdsdafasdas', '{"group_admin":1}', 1, 0, '2016-09-02 00:49:58', '2016-09-02 00:49:58');

--
-- Triggers `groups`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_groups` BEFORE INSERT ON `groups`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `question_type_id` varchar(200) NOT NULL,
  `question_category_id` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `question_text` varchar(200) NOT NULL,
  `question_choices` varchar(200) NOT NULL,
  `points` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `uuid`, `question_type_id`, `question_category_id`, `title`, `question_text`, `question_choices`, `points`, `created_by`, `modified_by`, `updated_at`, `created_at`, `is_deleted`) VALUES
(1, 'd76c98e6-705f-11e6-8b17-005056b1433f', '1', '1', 'Company Name', 'What is the new name of your company?', '', '10.00', 0, 0, '2016-09-01 19:37:38', '2016-09-01 16:19:21', 0),
(2, '4a20f592-7061-11e6-8b17-005056b1433f', '1', '1', '', 'sample test 13', '', '10.00', 0, 0, '2016-09-01 16:29:43', '2016-09-01 16:29:43', 0),
(3, '8b3c7449-7067-11e6-8b17-005056b1433f', '1', '1', '', 'sample test 13', '', '10.00', 0, 0, '2016-09-01 17:14:30', '2016-09-01 17:14:30', 0);

--
-- Triggers `questions`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_questions` BEFORE INSERT ON `questions`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `question_category`
--

CREATE TABLE IF NOT EXISTS `question_category` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `modified_by` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_category`
--

INSERT INTO `question_category` (`id`, `uuid`, `name`, `created_by`, `modified_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, '329315f0-7049-11e6-8b17-005056b1433f', 'Englis - edit1', 'Administrator', 'Administrator', '2016-09-01 13:37:16', '2016-09-01 13:37:49', 0),
(2, 'c02d4706-7049-11e6-8b17-005056b1433f', 'Filipino', 'Administrator', 'Administrator', '2016-09-01 13:41:14', '2016-09-01 13:41:14', 0);

--
-- Triggers `question_category`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_question_category` BEFORE INSERT ON `question_category`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `question_type`
--

CREATE TABLE IF NOT EXISTS `question_type` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `code` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_type`
--

INSERT INTO `question_type` (`id`, `uuid`, `code`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, '095ac192-7049-11e6-8b17-005056b1433f', 'IDENTIF', 'Identification', 0, NULL, NULL),
(2, '095ac7c3-7049-11e6-8b17-005056b1433f', 'MULTICHOICE', 'Multiple Choice', 0, NULL, NULL),
(3, '095ac8eb-7049-11e6-8b17-005056b1433f', 'ESSAY', 'Essay', 0, NULL, NULL),
(4, '095ac9cf-7049-11e6-8b17-005056b1433f', 'SHORTANSWER', 'Short Answer', 0, NULL, NULL),
(5, '095acaab-7049-11e6-8b17-005056b1433f', 'TOF', 'True or False', 0, NULL, NULL);

--
-- Triggers `question_type`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_question_type` BEFORE INSERT ON `question_type`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `passing_score` varchar(200) NOT NULL,
  `total_score` varchar(200) NOT NULL,
  `time_limit` varchar(255) NOT NULL,
  `datetime_start` varchar(200) NOT NULL,
  `datetime_end` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `is_deploy` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `uuid`, `category_id`, `title`, `description`, `passing_score`, `total_score`, `time_limit`, `datetime_start`, `datetime_end`, `created_by`, `update_by`, `is_deploy`, `updated_at`, `created_at`, `id_deleted`) VALUES
(3, 'b016453d-70a2-11e6-8b17-005056b1433f', 2, 'Valid Test', 'Valid Desc', '50.00', '120', '10:00:00', '09-02-2016 10:00:00', '09-02-2016 15:00:00', 1, 1, 0, '2016-09-02 00:17:52', '2016-09-02 00:17:52', 0);

--
-- Triggers `test`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_test` BEFORE INSERT ON `test`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tests_groups`
--

CREATE TABLE IF NOT EXISTS `tests_groups` (
  `test_id` tinyint(1) NOT NULL,
  `group_id` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_questions`
--

CREATE TABLE IF NOT EXISTS `test_questions` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_questions`
--

INSERT INTO `test_questions` (`id`, `uuid`, `test_id`, `question_id`, `updated_at`, `created_at`) VALUES
(1, '0655563f-7087-11e6-9722-120015b74692', 1, 8, '2016-09-01 14:59:32', '2016-09-01 14:59:32'),
(2, '067053f3-7087-11e6-9722-120015b74692', 1, 9, '2016-09-01 14:59:32', '2016-09-01 14:59:32'),
(3, '0682179a-7087-11e6-9722-120015b74692', 1, 10, '2016-09-01 14:59:32', '2016-09-01 14:59:32'),
(4, '74079ee9-708c-11e6-9722-120015b74692', 2, 8, '2016-09-01 15:38:24', '2016-09-01 15:38:24'),
(5, '74234d15-708c-11e6-9722-120015b74692', 2, 9, '2016-09-01 15:38:24', '2016-09-01 15:38:24'),
(6, '7436d0c5-708c-11e6-9722-120015b74692', 2, 10, '2016-09-01 15:38:24', '2016-09-01 15:38:24'),
(7, 'b01828a1-70a2-11e6-8b17-005056b1433f', 3, 2, '2016-09-02 00:17:52', '2016-09-02 00:17:52'),
(8, 'b018f363-70a2-11e6-8b17-005056b1433f', 3, 3, '2016-09-02 00:17:52', '2016-09-02 00:17:52');

--
-- Triggers `test_questions`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_test_questions` BEFORE INSERT ON `test_questions`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT '0',
  `suspended` tinyint(1) DEFAULT '0',
  `banned` tinyint(1) DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, '172.16.7.208', '0', 0, 0, NULL, NULL, NULL),
(2, 1, '172.16.7.156', '5', 1, 0, '2015-03-16 20:27:43', '2015-03-16 20:27:43', NULL),
(3, 1, '127.0.0.1', '0', 0, 0, NULL, NULL, NULL),
(4, 2, '::1', '0', 0, 0, NULL, NULL, NULL),
(5, 1, '::1', '1', 0, 0, '2015-03-16 01:13:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `type_id` tinyint(1) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `persist_code` varchar(255) DEFAULT NULL,
  `activated` tinyint(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `type_id`, `username`, `email`, `password`, `first_name`, `last_name`, `permissions`, `persist_code`, `activated`, `last_login`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, '0ae7f9ee-7070-11e6-8b17-005056b1433f', 1, 'chichay', 'chichay@hey.com', '$2y$10$.xNb/VfqZgAKQKKRbjWCP.Vr8R2pU4K6VX/3pAonn8Y1QaTMecmTm', 'clint', 'macoll', '{"super_admin":1}', '$2y$10$gZOhPSy0Q4GAW1q6JoMGW.QhPYXtaUiGU5fYf2otUmUSm/lBNMzoS', 1, '2016-09-01 21:07:39', 0, '2016-09-01 18:15:20', '2016-09-01 21:07:39'),
(12, '6ff70f61-7076-11e6-8b17-005056b1433f', 2, 'sampadmin', 'sampadmin@vibalgroup.com', '$2y$10$GCyLEZN9QTsfOTNNOAVY2Oa8M5c0UOT6mNAo7lxg6K3b7baL7wH6G', 'Samp', 'Admin', '{"group_admin":1}', NULL, 1, NULL, 0, '2016-09-01 19:01:06', '2016-09-01 19:01:06'),
(13, '6cff76ea-7087-11e6-8b17-005056b1433f', 2, 'clintot', 'clin@hey.com', '$2y$10$w19lMX33sFPAgO6HfZUDz.DRMimcJ/cd56n/JqH5zSY4HaSJJfFgK', 'clint', 'macoll', '{"group_admin":1}', NULL, 1, NULL, 0, '2016-09-01 21:02:43', '2016-09-01 21:02:43'),
(14, '9e2dedf9-70eb-11e6-a7c1-00ff8f6b6fcd', 2, 'hey', 'hey@hey.com', '$2y$10$dUM21Vb45EAU3kAhwNcRgOJrzolcAdF9fbM.WYj87fT3oRKyoCM5K', 'clint', 'macoll', '{"group_admin":1}', NULL, 1, NULL, 0, '2016-09-02 02:59:55', '2016-09-02 02:59:55'),
(15, '1a1bdc4c-70ec-11e6-a7c1-00ff8f6b6fcd', 3, 'clint', 'clintelligent182@gmail.com', '$2y$10$wldwzs5NHlMIMCDLeUNEQOjLfn5N5cy4yK288XjJmaNWAgdKe7hNq', 'Clint', 'Ma. Coll', NULL, NULL, 1, NULL, 0, '2016-09-02 03:03:23', '2016-09-02 03:03:23');

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_users` BEFORE INSERT ON `users`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(11, 2),
(15, 2),
(12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `uuid`, `code`, `name`, `is_deleted`) VALUES
(1, '74f5923f-705c-11e6-8b17-005056b1433f', 'SUPERADMIN', 'Super Administrator', 0),
(2, '74f597a1-705c-11e6-8b17-005056b1433f', 'GROUPADMIN', 'Group Administrator', 0),
(3, '74f59912-705c-11e6-8b17-005056b1433f', 'EXAMINER', 'Group Examiner', 0),
(4, '74f59a30-705c-11e6-8b17-005056b1433f', 'EXAMINEE', 'Examinee', 0);

--
-- Triggers `user_type`
--
DELIMITER $$
CREATE TRIGGER `init_uuid_user_type` BEFORE INSERT ON `user_type`
 FOR EACH ROW SET NEW.uuid = UUID(  )
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `guid` (`uuid`);

--
-- Indexes for table `question_category`
--
ALTER TABLE `question_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `guid` (`uuid`);

--
-- Indexes for table `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_questions`
--
ALTER TABLE `test_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users1_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_has_groups_groups1_idx` (`group_id`),
  ADD KEY `fk_users_has_groups_users1_idx` (`user_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `question_category`
--
ALTER TABLE `question_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `test_questions`
--
ALTER TABLE `test_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
