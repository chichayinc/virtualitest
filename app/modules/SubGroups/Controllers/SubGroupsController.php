<?php

namespace SubGroups\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Sectors;
use \Branches;


class SubGroupsController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        Menu::get('admin_sidebar')->setActiveMenu('subgroups');
    }

    public function index()
    {
        $this->data['title'] = 'Sub-group Management';
        $user = Sentry::getUser();
        $sector = Sectors::find($user->sector_id);

        $this->data['groups'] = Branches::where('sector_id', '=', $sector->id)->get();
        View::display('@subgroups/index.twig', $this->data);
    }

    public function addSubGroup(){

        $group = new Branches;

        $group->sector_id = Input::post('sector_id');
        $group->name = Input::post('group_name');
        $group->save();

        App::flash('message_status', true);
        App::flash('message', 'Group created successfully');

        Response::redirect($this->siteUrl('admin/subgroups'), 302);
    }

}