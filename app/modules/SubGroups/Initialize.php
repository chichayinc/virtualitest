<?php

namespace SubGroups;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'SubGroups';
    }

    public function getModuleAccessor(){
        return 'subgroups';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $userGroup = $adminMenu->createItem('subgroups', array(
            'label' => 'Sub Group Management',
            'icon'  => 'group',
            'url'   => 'admin/subgroups'
        ));

        $adminMenu->addItem('subgroups', $userGroup);
    }

    public function registerAdminRoute(){
        Route::resource('/subgroups', 'SubGroups\Controllers\SubGroupsController');
<<<<<<< HEAD
=======
        Route::post('/subgroup/add', 'SubGroups\Controllers\SubGroupsController::addSubGroup')->name('add_subgroup');
>>>>>>> c7a2aac9fcc81ee8bc29d7a8b7ea4f92f8700132
    }
}