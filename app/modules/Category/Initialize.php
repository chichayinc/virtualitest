<?php

namespace Category;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'Category';
    }

    public function getModuleAccessor(){
        return 'category';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $userGroup = $adminMenu->createItem('categories', array(
            'label' => 'Category Management',
            'icon'  => 'book',
            'url'   => 'admin/categories'
        ));

        $adminMenu->addItem('categories', $userGroup);
    }

    public function registerAdminRoute(){
        Route::resource('/categories', 'Category\Controllers\CategoryController');
    }
}