<?php

namespace Category\Controllers;

use \Category;
use \Input;
use \Request;
use \Response;
use \View;
use \Menu;
use \Admin\BaseController;

class CategoryController extends BaseController {

	public function __construct()
    {
        parent::__construct();
        Menu::get('admin_sidebar')->setActiveMenu('category');
        $this->loadJs('app/site.js');
        $this->loadJs('app/crud.js');
    }

	public function index(){
		$this->data['title'] = 'Manage Categories';
		$this->data['model_name'] = 'categories';
        $this->data['create_is_allowed'] = 1; 
        $this->data['update_and_delete_is_allowed'] = 1; 
        $this->data['table_column_names'] = [
            'id' => '#', 
            'sector_id' => 'Username', 
            'name' => 'Name'
        ];

		if (Request::isAjax()){
			$this->show($id);
		} else {
			View::display('@category/index.twig', $this->data);
		}

	}

	public function show($id){

		$category = null;

		if (null === $id){
			$category = Category::where('is_deleted', 0)->get();
			$this->data['records'] = $category->toArray(); 
		} else {
			$category = Category::find($id);
		}

		return [
			'success' => $category ? true : false,
			'data' => $category,
			'message' => $category ? 'Alright!' : 'Not Found',
			'code' => $category ? 200 : 400
		];
		
	}

	public function store(){
		
		$category = new Category;
		$input = Input::post();

		$category->fill($input);
		$category->save();

		$responseBody = [
			'success' => $category ? true : false,
			'data' => $category,
			'message' => $category ? 'Alright!' : 'Server Error',
			'code' => $category ? 200 : 500
		];


		Response::headers()->set('Content-Type', 'application/json');
        Response::setBody(json_encode($responseBody));    
	}

	public function update($id){

		$category = Category::find($id);
		$input = Input::post();

		$category->fill($input);
		$category->save();

		$responseBody = [
			'success' => $category ? true : false,
			'data' => $category,
			'message' => $category ? 'Alright!' : 'Server Error',
			'code' => $category ? 200 : 500
		];


		Response::headers()->set('Content-Type', 'application/json');
        Response::setBody(json_encode($responseBody));    
	}

	public function destroy($id){
		$category = Category::find($id);

		$category->is_deleted = 1;
		$category->save();

		$responseBody = [
			'success' => $category ? true : false,
			'data' => $category,
			'message' => $category ? 'Alright!' : 'Server Error',
			'code' => $category ? 200 : 500
		];


		Response::headers()->set('Content-Type', 'application/json');
        Response::setBody(json_encode($responseBody));    

	}

}