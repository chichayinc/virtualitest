<?php

namespace Groups\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \SuperAdmin\BaseController;
use \Sectors;


class GroupsController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        Menu::get('superadmin_sidebar')->setActiveMenu('groups');
    }

    public function index()
    {
        $this->data['title'] = 'Group Management';
        $this->data['groups'] = Sectors::all();
        /** render the template */
        View::display('@groups/index.twig', $this->data);
    }

    public function addGroup(){
        $group = new Sectors;

        $group->name = Input::post('group_name');
        $group->save();

        App::flash('message_status', true);
        App::flash('message', 'Group created successfully!');

        Response::redirect($this->siteUrl('superadmin/groups'));
    }

    public function addAdmin(){
        try
        {
            // Create the user
            $user = Sentry::createUser(array(
                'username'  => Input::post('username'),
                'email'     => Input::post('email'),
                'password'  => Input::post('password'),
                'first_name'  => Input::post('first_name'),
                'last_name'  => Input::post('last_name'),
                'sector_id'  => Input::post('sector_id'),
                'activated' => true,
            ));

            // Find the group using the group id
            $adminGroup = Sentry::findGroupById(2);

            // Assign the group to the user
            $user->addGroup($adminGroup);

            App::flash('message_status', true);
            App::flash('message', 'Admin added successfully!');
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            App::flash('message_status', false);
            App::flash('message', 'Username is required.');
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            App::flash('message_status', false);
            App::flash('message', 'Password field is required.');
        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            App::flash('message_status', false);
            App::flash('message', 'User with this login already exists.');

        }
        catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
        {
            App::flash('message_status', false);
            App::flash('message', 'Group was not found');
        }

        Response::redirect($this->siteUrl('superadmin/groups'));
    }
}