<?php

namespace Groups;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'Groups';
    }

    public function getModuleAccessor(){
        return 'groups';
    }

    public function registerSuperAdminMenu(){

        $adminMenu = Menu::get('superadmin_sidebar');

        $userGroup = $adminMenu->createItem('groups', array(
            'label' => 'Group Management',
            'icon'  => 'group',
            'url'   => 'superadmin/groups'
        ));

        $adminMenu->addItem('groups', $userGroup);
    }

    public function registerSuperAdminRoute(){
        Route::resource('/groups', 'Groups\Controllers\GroupsController');
        Route::post('/group/add', 'Groups\Controllers\GroupsController:addGroup')->name('add_group');
        Route::post('/admin/add', 'Groups\Controllers\GroupsController:addAdmin')->name('add_admin');
    }
}