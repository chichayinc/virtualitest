<?php

namespace Users;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'Users';
    }

    public function getModuleAccessor(){
        return 'users';
    }

    public function registerSuperAdminMenu(){

        $adminMenu = Menu::get('superadmin_sidebar');

        $userGroup = $adminMenu->createItem('users', array(
            'label' => 'User Management',
            'icon'  => 'user',
            'url'   => 'superadmin/users'
        ));

        $adminMenu->addItem('users', $userGroup);
    }

    public function registerSuperAdminRoute(){
        Route::resource('/users', 'Users\Controllers\UsersController');
    }
}