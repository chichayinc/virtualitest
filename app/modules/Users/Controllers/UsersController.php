<?php

namespace Users\Controllers;

use \App;
use \View;
use \Menu;
use \SuperAdmin\BaseController;
use \Sentry;
use \User;

class UsersController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        Menu::get('superadmin_sidebar')->setActiveMenu('users');
    }

    public function index()
    {
        $this->data['title'] = 'User Management';
        $users = User::getUseWithGroups();
        $this->data['people'] = $users;

        /** render the template */
        View::display('@users/index.twig', $this->data);
    }
}