<?php

/* superadmin/dashboard.twig */
class __TwigTemplate_0f345e681854505b0ea32ac29a0f8c32b08e6af1709e11052b1aab9c0fbcd558 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        echo " 
<div class=\"row\">
    <div class=\"col-lg-12 text-center\">
        <h3 class=\"page-header\">Welcome to SlimStarter</h3>
        <p>Let's build your own dashboard!</p>
        <p>by editing app/views/admin/dashboard.twig</p>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "superadmin/dashboard.twig";
    }

    public function getDebugInfo()
    {
        return array (  20 => 1,  26 => 14,  24 => 4,  19 => 1,);
    }
}
