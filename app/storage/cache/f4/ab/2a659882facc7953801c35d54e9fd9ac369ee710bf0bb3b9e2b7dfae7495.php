<?php

/* admin/dashboard.twig */
class __TwigTemplate_f4ab2a659882facc7953801c35d54e9fd9ac369ee710bf0bb3b9e2b7dfae7495 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        echo " 
<div class=\"row\">
    <div class=\"col-lg-12 text-center\">
        <h3 class=\"page-header\">Welcome to SlimStarter</h3>
        <p>Let's build your own dashboard!</p>
        <p>by editing app/views/admin/dashboard.twig</p>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/dashboard.twig";
    }

    public function getDebugInfo()
    {
        return array (  20 => 1,  26 => 14,  24 => 4,  19 => 1,  71 => 14,  68 => 13,  63 => 6,  55 => 18,  51 => 16,  49 => 13,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  42 => 8,  39 => 5,  32 => 3,  29 => 2,);
    }
}
