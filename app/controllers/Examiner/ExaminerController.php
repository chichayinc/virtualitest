<?php

namespace Examiner;

use \App;
use \Menu;
use \Module;
use \User;
use \Sectors;
use \Sentry;

class ExaminerController extends BaseController
{

    /**
     * display the admin dashboard
     */
    public function index()
    {
        App::render('examiner/index.twig', $this->data);
    }
    

}