<?php
namespace Examiner;

use \App;
use \Menu;
use \Module;
use \Sectors;

class BaseController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

        $adminMenu = Menu::create('examiner_sidebar');
        
        $user = Sentry::getUser();
        $sector = Sectors::find($user->sector_id);
        $this->data['user'] = $user;
        $this->data['sector'] = $sector;

        foreach (Module::getModules() as $module) {
            $module->registerExaminerMenu();
        }

    }
}