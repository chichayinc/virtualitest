<?php

class UserController extends BaseController {

	public function index() {
		// App::render('user/index.twig', $this->data);
	}

	/**
	 * display the login form
	 */
	public function login() {

		if (Sentry::check()) {
			$user = Sentry::getUser();
			if($user->hasAnyAccess(['super_admin'])) {
				Response::redirect($this -> siteUrl('superadmin'));
			} elseif($user->hasAnyAccess(['admin'])){
				Response::redirect($this -> siteUrl('admin'));
			}elseif($user->hasAnyAccess(['examiner'])){
				Response::redirect($this -> siteUrl('examiner'));
			}elseif($user->hasAnyAccess(['examinee'])){
				Response::redirect($this -> siteUrl('examinee'));
			}else{
				Response::redirect($this -> siteUrl('member'));
			}
		} else {
			$this -> data['redirect'] = (Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '';
			$this -> publish('baseUrl', $this -> data['baseUrl']);
			View::display('user/login.twig', $this -> data);
		}

	}

	public function logout() {
		Sentry::logout();
		Response::redirect($this -> siteUrl('/'));
	}

	public function createUser(){

		$body = Input::getBody();
        $data = json_decode($body);

        $username = $data->username;
        $password = $data->password;
        $user_type = $data->user_type;
        $source_id = $data->source_id;

		$user = Sentry::createUser(array(
		    'username'    => $username,
		    'password' => $password,
		    'user_type' => $user_type,
		    'source_id' => $source_id
		));

		$response["success"] = true;
        $response["message"] = 'User successfully added';
        $response["code"] = '200';
        $response["data"] = $user;


        Response::headers()->set('Content-Type', 'application/json');
        Response::setBody(json_encode($response));

	}

	public function doLogin() {
		$username = Input::post('username');
		$password = Input::post('password');

		try
	        {
	            // Login credentials
	            $credentials = array(
	                'username'    => $username,
	                'password' => $password
	            );

	            // Authenticate the user
	            $user = Sentry::authenticate($credentials, false);

	            if ($user) {
	            	Sentry::login($user, false);
	            	App::flash('message', 'Login successful.');
					App::flash('username', $username);
					Response::redirect($this -> siteUrl('/'));
	            }

	        }
	        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
	        {
	            App::flash('message', 'Login field is required.');
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/'));
	        }
	        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
	        {
	            App::flash('message', 'Password field is required.');
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/'));
	        }
	        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
	        {
	            App::flash('message', 'Wrong password, try again.');
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/'));
	        }
	        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	        {
	            App::flash('message', 'User was not found.');
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/'));
	        }
	        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
	        {   
	            App::flash('message', 'User is not activated.');
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/'));
	        }
	        catch(\Exception $e) {
				App::flash('message', $e -> getMessage());
				App::flash('username', $username);

				Response::redirect($this -> siteUrl('/'));
			}

	}

}	