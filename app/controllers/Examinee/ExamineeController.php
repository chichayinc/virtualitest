<?php

namespace Examinee;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class ExamineeController extends BaseController
{

    /**
     * display the admin dashboard
     */
    public function index()
    {
        App::render('examinee/index.twig', $this->data);
    }

}