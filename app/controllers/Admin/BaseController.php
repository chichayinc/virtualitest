<?php
namespace Admin;

use \App;
use \Menu;
use \Module;
use \User;
use \Sectors;
use \Sentry;

class BaseController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

        $adminMenu = Menu::create('admin_sidebar');

        $user = Sentry::getUser();
        $sector = Sectors::find($user->sector_id);
        $this->data['user'] = $user;
        $this->data['sector'] = $sector;

        foreach (Module::getModules() as $module) {
            $module->registerAdminMenu();
        }

    }
}