<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class AdminController extends BaseController
{

    /**
     * display the admin dashboard
     */
    public function index()
    {
        Response::redirect($this -> siteUrl('admin/subgroups'));
    }

}