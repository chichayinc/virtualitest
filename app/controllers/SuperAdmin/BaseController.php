<?php
namespace SuperAdmin;

use \App;
use \Menu;
use \Module;
use \Sentry;
use \Sectors;

class BaseController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';
        $user = Sentry::getUser();
        $this->data['user'] = $user;

        $adminMenu = Menu::create('superadmin_sidebar');

        foreach (Module::getModules() as $module) {
            $module->registerSuperAdminMenu();
        }

    }
}