<?php

namespace Member;

use \App;
use \Menu;
use \Module;
use \Sentry;

use \MembersProfiles;
use \DirectReferrals;

class BaseController extends \BaseController {
	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');

		foreach (Module::getModules () as $module) {
			$module -> registerMemberMenu();
		}
	}

}
