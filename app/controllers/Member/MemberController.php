<?php

namespace Member;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;

class MemberController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		/**
		 * publish necessary js variable
		 */
		$this -> publish('baseUrl', $this -> data['baseUrl']);

		Response::Redirect($this -> siteUrl('member/myaccount'));
	}

}
