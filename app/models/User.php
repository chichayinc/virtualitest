<?php

class User extends Model {
protected $table = 'users';

	public function getUseWithGroups(){
		return User::leftJoin('users_groups as ug', 'ug.user_id', '=', 'users.id')
					->leftJoin('sectors as s', 's.id', '=', 'users.sector_id')
					->leftJoin('groups as g', 'g.id', '=', 'ug.group_id')
					->select(['*','users.created_at as created_at', 'g.name as group_name', 's.name as sector_name'])
			        ->where('users.id', '!=', 1)
			        ->orderBy('users.created_at', 'DESC')
			        ->get();
	}

}