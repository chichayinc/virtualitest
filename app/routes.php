<?php

/**
 * Sample group routing with user check in middleware
 */
Route::group('/superadmin', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if (!$user->hasAnyAccess(['super_admin'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'SuperAdmin\SuperAdminController:index') -> name('superadmin');
    foreach (Module::getModules() as $module) {
        $module -> registerSuperAdminRoute();
    }
});

Route::group('/admin', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if (!$user->hasAnyAccess(['admin'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Admin\AdminController:index') -> name('admin');
    foreach (Module::getModules() as $module) {
        $module -> registerAdminRoute();
    }
});

Route::group('/examiner', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if (!$user->hasAnyAccess(['examiner'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Examiner\ExaminerController:index') -> name('examiner');
    foreach (Module::getModules() as $module) {
        $module -> registerExaminerRoute();
    }
});

Route::group('/examinee', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if (!$user->hasAnyAccess(['examinee'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Examinee\ExamineeController:index') -> name('examinee');
    foreach (Module::getModules() as $module) {
        $module -> registerExamineeRoute();
    }
});

Route::group('/member', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if ($user->hasAnyAccess(['admin_portal'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }   else if ($user->hasAnyAccess(['load_portal'])) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Member\MemberController:index') -> name('member');
    foreach (Module::getModules() as $module) {
        $module -> registerPublicRoute();
    }
});


// Route::get('/login', 'Admin\AdminController:login');
// Route::get('/logout', 'Admin\AdminController:logout')->name('logout');
// Route::post('/login', 'Admin\AdminController:doLogin');

/** default routing */
Route::get('/', 'UserController:login')->name('login');
Route::get('/logout', 'UserController:logout')->name('logout');
Route::post('/createuser', 'UserController:createUser');
Route::post('/login', 'UserController:doLogin')->name('dologin');