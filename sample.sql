-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 01, 2016 at 01:31 PM
-- Server version: 5.7.13
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vsmart_licensing`
--

-- --------------------------------------------------------

--
-- Table structure for table `activated_licenses`
--

CREATE TABLE IF NOT EXISTS `activated_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `license_code` varchar(25) NOT NULL,
  `serial_number` varchar(25) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `app_types`
--

CREATE TABLE IF NOT EXISTS `app_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `app_types`
--

INSERT INTO `app_types` (`id`, `code`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'BGYU6TYRS5', 'LMS', 'LMS', 0, '2016-06-24 04:02:22', NULL, '2016-06-24 04:02:22', NULL),
(2, '6YF4WW85IY', 'School', 'LMS for School', 0, '2016-06-24 04:02:33', NULL, '2016-06-24 04:02:33', NULL),
(3, 'LMMLPM448M', 'Work', 'LMS for Work', 0, '2016-06-24 04:02:41', NULL, '2016-06-24 04:02:41', NULL),
(4, '75T1FAUIT4', 'testtt', 'hey', 0, '2016-08-02 01:42:02', NULL, '2016-08-02 01:42:02', NULL),
(5, 'XZ1FCWZALB', 'aaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbb', 0, '2016-08-02 01:42:26', NULL, '2016-08-02 01:42:26', NULL),
(6, 'CUQ8JKWD6I', '', '', 0, '2016-08-02 01:48:16', NULL, '2016-08-02 01:48:16', NULL),
(7, '27XKLCXCWR', '', '', 0, '2016-08-02 02:34:48', NULL, '2016-08-02 02:34:48', NULL),
(8, '22GQVGDIM7', 'ccc', 'cccc', 0, '2016-08-02 02:35:03', NULL, '2016-08-02 02:35:03', NULL),
(9, 'A666VQQ9I8', 'ccc', 'cccc', 0, '2016-08-02 02:35:11', NULL, '2016-08-02 02:35:11', NULL),
(10, 'TQX74CM2D2', '11111', '1111', 0, '2016-08-02 02:35:43', NULL, '2016-08-02 02:35:43', NULL),
(11, 'TJBFKB9WW7', '333', '3333', 0, '2016-08-02 02:36:44', NULL, '2016-08-02 02:36:44', NULL),
(12, '44GM1VBAL1', '333', '3333', 0, '2016-08-02 02:36:52', NULL, '2016-08-02 02:36:52', NULL),
(13, 'IRYA3PSU7Z', '56445665465', '5645665', 0, '2016-08-02 02:37:48', NULL, '2016-08-02 02:37:48', NULL),
(14, 'KY7IP8AD76', '', '', 0, '2016-08-02 02:39:13', NULL, '2016-08-02 02:39:13', NULL),
(15, 'HCTMA9ABTK', '', '', 0, '2016-08-02 02:39:18', NULL, '2016-08-02 02:39:18', NULL),
(16, '68GLHYHSI3', 'asfdadssda', 'sdfsdf', 0, '2016-08-02 02:39:26', NULL, '2016-08-02 02:39:26', NULL),
(17, 'S7ZRQDNCSH', '', '', 0, '2016-08-02 02:39:51', NULL, '2016-08-02 02:39:51', NULL),
(18, '8UR4QYDQ2G', 'aaaaaaaaaaaa', 'aaaaaaaa', 0, '2016-08-02 02:40:07', NULL, '2016-08-02 02:40:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `code`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'YA6ELC5ZEZ', 'Ullen Faustino', 'Software IT Company', 0, '2016-06-24 02:44:30', NULL, '2016-06-24 02:44:30', NULL),
(2, 'WCM3BVQQYY', 'PLAYSTATION', 'PLAYSTATION', 0, '2016-06-27 10:20:40', NULL, '2016-06-27 10:20:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', '{"admindashboard" : 1, "users_management":1,"clients":1,"modules":1,"packages":1,"apptypes":1,"licenses":1}', '2016-06-15 10:25:05', '2016-06-15 10:25:05');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE IF NOT EXISTS `licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `hash_code` varchar(125) DEFAULT NULL,
  `client_code` varchar(25) NOT NULL,
  `app_types_code` varchar(25) NOT NULL,
  `packages_code` varchar(25) NOT NULL,
  `max_users` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `code`, `hash_code`, `client_code`, `app_types_code`, `packages_code`, `max_users`, `count`, `expiration`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '6DEBJIXNDF', '9U4W7AS7-946B4YZ7-4G4A12SU', 'YA6ELC5ZEZ', 'BGYU6TYRS5', '5RXBQJAC5E', 1, 1, '2016-06-28 00:00:00', 0, '2016-06-28 07:48:43', NULL, '2016-06-28 07:48:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `license_modules`
--

CREATE TABLE IF NOT EXISTS `license_modules` (
  `licenses_code` varchar(25) NOT NULL,
  `modules_code` varchar(25) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`licenses_code`,`modules_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `license_modules`
--

INSERT INTO `license_modules` (`licenses_code`, `modules_code`, `created_at`, `updated_at`) VALUES
('6DEBJIXNDF', '5TWGGFR3S9', '2016-06-28 07:48:44', '2016-06-28 07:48:44'),
('6DEBJIXNDF', 'AXP3T9TZPS', '2016-06-28 07:48:44', '2016-06-28 07:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE IF NOT EXISTS `master_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 'eb0a191797624dd3a48fa681d3061212', 1, '2016-02-03 11:59:41', '2016-02-03 11:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `code`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'AXP3T9TZPS', 'Dashboard', 'Dashboard Module', 0, '2016-06-24 02:35:18', NULL, '2016-06-24 02:35:18', NULL),
(2, '5TWGGFR3S9', 'Users Management', 'Users Management', 0, '2016-06-24 10:55:30', NULL, '2016-06-27 02:36:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `code`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'HR7MAH1H9T', 'Demo', 'Demo Package', 0, '2016-06-24 02:34:43', NULL, '2016-06-24 02:34:43', NULL),
(2, '5RXBQJAC5E', 'Free', 'Free Package', 0, '2016-06-24 02:34:50', NULL, '2016-06-24 02:34:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_modules`
--

CREATE TABLE IF NOT EXISTS `package_modules` (
  `packages_code` varchar(25) NOT NULL,
  `modules_code` varchar(25) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`packages_code`,`modules_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_modules`
--

INSERT INTO `package_modules` (`packages_code`, `modules_code`, `created_at`, `updated_at`) VALUES
('5RXBQJAC5E', 'AXP3T9TZPS', '2016-06-27 02:55:26', '2016-06-27 02:55:26'),
('HR7MAH1H9T', '5TWGGFR3S9', '2016-06-27 02:55:23', '2016-06-27 02:55:23');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_throttle_users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT '',
  `middle_name` varchar(45) DEFAULT '',
  `last_name` varchar(255) DEFAULT '',
  `permissions` text,
  `activated` tinyint(1) DEFAULT '1',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `secondary_password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `middle_name`, `last_name`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `secondary_password`, `created_at`, `updated_at`) VALUES
(1, 'chichay', 'admin1@vibalgroup.com', '$2y$10$IsY9tLSXRbxnB1LgwkaxB.pSvv.t/unB7e35U1d1J1OypadlyiJ82', 'Admin', '', '', NULL, 1, '', NULL, '2016-08-02 02:55:15', '$2y$10$t3542nalfa7.dp6PAh.mtufPrWrp8R7t7aCjlCqzxNlf7otHxUXyW', NULL, NULL, '2016-06-15 10:25:05', '2016-08-02 02:55:15'),
(2, 'admin2', 'admin2@vibalgroup.com', '$2y$10$Gr8cMJmyEhn02K.N/zAYtOh1sHz9ttHpeLVvACR3Cp.YJk8lZkdWq', 'Admin', '', '', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, '2016-06-15 10:25:05', '2016-06-15 10:25:05'),
(3, 'admin3', 'admin3@vibalgroup.com', '$2y$10$NeV.UccODQdyoNaZOfWjBuJkTr4LgfHVmFki/Uf7mdu5h4YvC9LWO', 'Admin', '', '', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, '2016-06-15 10:25:05', '2016-06-15 10:25:05'),
(4, 'admin4', 'admin4@vibalgroup.com', '$2y$10$HPsKMLdxrz5Hn8G5ljBDtOMJZNgzgw1/S3I1gUQT3i5Yfu55c5PNe', 'Admin', '', '', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, '2016-06-15 10:25:05', '2016-06-15 10:25:05'),
(5, 'admin5', 'admin5@vibalgroup.com', '$2y$10$Xemd1/LhRjznxmCquEFp9u1CG0uRqrGHsQ0tRcu4nCPmbd03Gb6m2', 'Admin', '', '', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, '2016-06-15 10:25:05', '2016-06-15 10:25:05');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `fk_users_group_groups1_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
